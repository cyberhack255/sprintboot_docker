#!/bin/bash

gradle bootJar

echo "Setting up Docker Network"
docker network create --driver=bridge guestbook

echo"Creating Mysql container"
docker run --name guestbook_db -p3306:3306 -v guestbook_db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=guestbook --network guestbook -d mysql:5.7
#3306 port to host exposure can be removed

echo"Creating inspector container"
docker run -dit --name guestbook_inspector --network guestbook alpine ash

echo" copy SQL file to the inspector container"
docker cp schema.sql guestbook_inspector:/tmp/schema.sql

echo" Attach STDIN,STDOUT and STDERR to the container"
docker container attach guestbook_inspector

echo " installing mysql-client and curl on inspector container"
apk update && apk upgrade && apk add mysql-client curl

echo " Connect to mysql db"
mysql -h guestbook_db -u root -p guestbook < /tmp/schema.sql

echo "Deploying the webapp"
echo "Stopping all running guestbook webapp container"
docker container stop guestbook && docker container rm guestbook || true

echo "login to gitlab to pull the latest container"
docker login registry.gitlab.com
docker pull registry.gitlab.com/cyberhack255/guestbook:latest
docker run -d -p 80:8080 --name guestbook --network guestbook registry.gitlab.com/cyberhack255/guestbook:latest

#for running the webapp with no pass on mysql server
docker run -d -p 80:8080 --name guestbook --network guestbook registry.gitlab.com/cyberhack255/guestbook:nopassmysql
